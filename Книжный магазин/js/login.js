  'use strict'

  const loginLink = document.querySelector('.form__login');
  const loginSendBtn = document.querySelector('.form__login-send');

  const notExistUserMsg = 'Пользователя с такими данными не существует';
  const notRigthEmailOrPasswordMsg = 'Email или Пароль невереный';

  loginLink.addEventListener('click', () => {
    removeCheckResult();

    const loginLink = document.querySelector('.form__login');
    const titleRegistration = document.querySelector('.form__registration-title');
    const agreeRegistration = document.querySelector('.form__registration-agree');
    const sendRegistration = document.querySelector('.form__registration-send');    
    
    loginLink.style.display = 'none';
    titleRegistration.style.display = 'none';
    agreeRegistration.style.display = 'none';
    sendRegistration.style.display = 'none';

    const registrationLink = document.querySelector('.form__registration');
    const titleLogin = document.querySelector('.form__login-title');
    const agreeLogin = document.querySelector('.form__login-agree');
    const sendLogin = document.querySelector('.form__login-send');

    registrationLink.style.display = 'block';
    titleLogin.style.display = 'block';
    agreeLogin.style.display = 'block';
    sendLogin.style.display = 'block';

    emailInput.value = 'johndoe@email.com';
    passwordInput.value = 'Введите пароль';
  });

  loginSendBtn.addEventListener('click', () => {
    removeErrorMsgElems();

    if (checkFields()) {
      let dataStr = localStorage.getItem('usersData');
      if (!dataStr) {
        alert(notExistUserMsg);
        return;
      }

      let data = JSON.parse(dataStr);
      let email = document.querySelector('.form__input.email');
      let existUser = data.find((val) => val.email == email.value);

      if (existUser) {
        let password = document.querySelector('.form__input.password');

        if (password.value == existUser.password) {
          window.location.href = 'https://learn.javascript.ru/localstorage';
        } else {
          let email = document.getElementsByClassName('email');
          let password = document.getElementsByClassName('password');

          handlingError(email, true);
          handlingError(password, true, notRigthEmailOrPasswordMsg);
        }
      } else {
        alert(notExistUserMsg);
      }

      isValidate = true;
    }
  });