function imgElement() {
  var newImg = "url('./image/you-dont-know-js.jpg')";
  document.body.style.backgroundImage = newImg;
  console.log("newImg:", newImg);
}

document.addEventListener("DOMContentLoaded", function() {
  var newBackgroundImage = "url('./image/you-dont-know-js.jpg')";

  document.body.style.backgroundImage = newBackgroundImage;
});

document.addEventListener("DOMContentLoaded", function() {
  var books = Array.from(document.querySelectorAll(".book"));

  books.sort(function(a, b) {
    var titleA = a.querySelector("h2").innerText;
    var titleB = b.querySelector("h2").innerText;
    return titleA.localeCompare(titleB);
  });

  var booksContainer = document.querySelector(".books");
  books.forEach(function(book) {
    booksContainer.appendChild(book);
  });
});

document.addEventListener("DOMContentLoaded", function() {
  var thirdHeader = document.getElementById("thirdHeader");

  if (thirdHeader) {
    thirdHeader.innerText = 'Получится - "Книга3. this и Прототипы Объектов"';
  }
});

document.addEventListener("DOMContentLoaded", function() {
  var adElement = document.querySelector(".adv");

  if (adElement) {
    adElement.remove();
  }
});

// ИЗВЕНИТЕ У МЕНЯ НЕ ПОЛУЧИЛОСЬ УПОРЯДИТЬ ГЛАВЫ!!!((( Я ПЫТАЛСЯ ЧЕСТНО!!

// const secondBookChapters = Array.from(
//   document.querySelectorAll(".book:nth-child(2) li.chapter")
// );

// function getChapterText(chapterElement) {
//   return chapterElement.textContent.trim();
// }

// secondBookChapters.sort((a, b) => {
//   const textA = getChapterText(a);
//   const textB = getChapterText(b);
//   return textA.localeCompare(textB, "ru", { sensitivity: "base" });
// });

// secondBookChapters.forEach(chapter => chapter.remove());

// const secondBook = document.querySelector(".book:nth-child(2) ul");
// secondBookChapters.forEach(chapter => secondBook.appendChild(chapter));
