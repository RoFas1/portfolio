  'use strict'

  const registerLink = document.querySelector('.form__registration');
  const registrationSendBtn = document.querySelector('.form__registration-send');

  registerLink.addEventListener('click', () => {
    removeCheckResult();

    const registrationLink = document.querySelector('.form__registration');
    const titleLogin = document.querySelector('.form__login-title');
    const agreeLogin = document.querySelector('.form__login-agree');
    const sendLogin = document.querySelector('.form__login-send');

    registrationLink.style.display = 'none';
    titleLogin.style.display = 'none';
    agreeLogin.style.display = 'none';
    sendLogin.style.display = 'none';

    const loginLink = document.querySelector('.form__login');
    const titleRegistration = document.querySelector('.form__registration-title');
    const agreeRegistration = document.querySelector('.form__registration-agree');
    const sendRegistration = document.querySelector('.form__registration-send');    
    
    loginLink.style.display = 'block';
    titleRegistration.style.display = 'block';
    agreeRegistration.style.display = 'block';
    sendRegistration.style.display = 'block';

    emailInput.value = 'Введите email';
    passwordInput.value = 'Введите пароль';
  });

  registrationSendBtn.addEventListener('click', () => {
    removeErrorMsgElems();

    const agreeElemForCheck = document.querySelector('.agree');
    checkCheckbox(agreeElemForCheck);

    if (checkFields()) {
      let usersData = [];

      let user = {
        email: "",
        password: ""
      };

      let email = document.querySelector('.form__input.email');
      let password = document.querySelector('.form__input.password');

      if (localStorage.getItem('usersData')) {
        let dataStr = localStorage.getItem('usersData');
        let data = JSON.parse(dataStr);
        let existUser = data.find((val) => val.email == email.value);

        if (!existUser) {
          user.email = email.value;
          user.password = password.value;
          data.push(user);
          localStorage.setItem('usersData', JSON.stringify(data));
        } else {
          existUser.password = password.value;
          localStorage.setItem('usersData', JSON.stringify(data));
        }
      } else {
        user.email = email.value;
        user.password = password.value;
        usersData.push(user);
        localStorage.setItem('usersData', JSON.stringify(usersData));
      }

      email.value = "";
      password.value = "";
    }

    isValidate = true;
  });