  'use strict'

  const emailInput = document.querySelector('.form__input.email');
  const passwordInput = document.querySelector('.form__input.password');

  const fieldsForCheck = ['email', 'password', 'agree'];
  const regexp = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu
  let isValidate = true;

  const registrationMandatoryFields = document.querySelectorAll('.form__mandatory-field > .form__input');
    registrationMandatoryFields.forEach(elem => {
      elem.addEventListener('click', e => {
        if (e.target.value == 'johndoe@email.com' || 
        e.target.value == 'Введите пароль' || 
        e.target.value == 'Введите email') {
          e.target.value = "";
        }
    });
  });

  const removeErrorMsgElems = () => {
    let errorMsgElems = document.querySelectorAll('.error-msg');
    if (errorMsgElems) {
      errorMsgElems.forEach(e => {
        e.parentElement.removeChild(e);
      });
    }
  };

  const removeCheckResult = () => {
    const elementsForCheck = document.querySelectorAll('.form__input');

    elementsForCheck.forEach(elemForCheck => {
      let className = fieldsForCheck.find(val => elemForCheck.classList.contains(val));
      let elementsForShowError = document.getElementsByClassName(className);

      handlingError(elementsForShowError, false);
    });

    removeErrorMsgElems();
  };

  const createRegistrationErrorMsgElem = (e, message) => {
    const errorMsgElem = document.createElement("span");
    errorMsgElem.textContent = message;
    errorMsgElem.classList.add('error-msg');
    e.after(errorMsgElem);
  };

  const handlingError = (elementsForShowError, isShowError, message) => {
    if (isShowError) {
      Array.from(elementsForShowError).forEach(e => {
          if (e.classList.contains('form__input')) {
            e.classList.add('input-error');
            createRegistrationErrorMsgElem(e, message);
          } else {
            e.classList.add('text-error');
          }
      });
    } else {
      Array.from(elementsForShowError).forEach(e => {
        if (e.classList.contains('form__input')) {
          e.classList.remove('input-error');
        } else {
          e.classList.remove('text-error');
        }
      });
    }
  };

  const checkMandatoryField = (elemForCheck, elementsForShowError) => {
    if (elemForCheck.value.length == 0) {
      handlingError(elementsForShowError, true, 'Поле обязательно для заполнения')
    } else {
      handlingError(elementsForShowError, false);
    }
  };

  const validFields = (elemForCheck, elementsForShowError, className) => {
    const content = elemForCheck.value;
    if (content.length == 0) {
      return;
    }

    if (className == 'email') {
      if (!regexp.test(content)) {
        handlingError(elementsForShowError, true, 'Некорректный email');
      } else {
        handlingError(elementsForShowError, false);
      }
    }

    if (className == 'password') {
      if (className == 'password' && content.length < 8) {
        handlingError(elementsForShowError, true, 'Пароль должен содержать минимум 8 символов');
      } else {
        handlingError(elementsForShowError, false);
      }
    }
  };

  const checkCheckbox = (elem) => {
    if (!elem.checked) {
      isValidate = false;
      elem.classList.add('input-error');
      createRegistrationErrorMsgElem(elem, 'Поле обязательно для заполнения');
    } else {
      elem.classList.remove('input-error');
      removeErrorMsgElems();
    }
  }
  
  const checkFields = () => {
    const elementsForCheck = document.querySelectorAll('.form__input');

    elementsForCheck.forEach(elemForCheck => {
      let className = fieldsForCheck.find(val => elemForCheck.classList.contains(val));
      let elementsForShowError = document.getElementsByClassName(className);

      checkMandatoryField(elemForCheck, elementsForShowError);
      validFields(elemForCheck, elementsForShowError, className);

      if (elemForCheck.classList.contains(['input-error'])) {
        isValidate = false;
      }
    });

    return isValidate;
  };